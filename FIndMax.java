
public class FIndMax {
	private static int temp[];
	
	/*Method that fine max factor of a number in an array*/
	public static int  find(int value[])
	{
		temp = value;
		int maximum = temp[0];// assuming first element is max 
		for (int i = 0; i < 10001; i++)
			{
				if (temp[i] > maximum)
					maximum = temp[i];
			}
		return maximum;
	}

}
