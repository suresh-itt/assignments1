

public class Factors extends Thread
	{
		private int lower, upper; //Stores the Upper and Lower Value 

		public Factors(int lower, int upper)
			{
				this.lower = lower;
				this.upper = upper;
			}
/*Method Contains the logic for execution in Threads */
		public synchronized void run()
			{

				for (int i = lower; i <= upper; i++)
					{
						for (int j = 1; j <= i; j++)
							{
								if (i % j == 0)
									{
									FactorFinding.a[i]++;
																			}
							}
					}
			}

	}
