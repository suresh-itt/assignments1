
public class FactorFinding {
	
	static  int[] a = new int[10001];
	public static void main(String[] args) throws InterruptedException
		{
			Thread t[] = new Thread[11];

			for (int k = 1; k < 11; k++)
				{
					t[k] = new Thread(     						//Creates the multiple threads 
						   new Factors(((k - 1) * 1000 + 1),k * 1000));	//creates new thread
					t[k].start();

				}
			/*thread terminate in the sequenctional manner */
			for (int k = 1; k < 11; k++)
				{
					t[k].join();
				}
			int max = findMax(a);  //calling funtion which find max form the factor stored in array
			
			System.out.println("Following numbers have higest number of Divisors:-");
			
			/*Checking if factor of numbers are equal and display them*/
			
			for (int i = 1; i < 10001; i++)
				{
					if (max == a[i])
						System.out.println("Number " + i+ "\t Number of Factors" + (max-2));
				}
		}
	/*method that find max from the array where factors(counting) are stored*/
	public static int findMax(int[] a)
	{
		int max = a[0];
		for (int i = 0; i < 10001; i++)
			{
				if (a[i] > max)
					max = a[i];
			}
		return max;
	}

}
