import java.util.Scanner;

class runner extends Thread
{
	
	int count = 0,i;
	
    runner(int i)
    {
    	this.i = i;
    }
	public synchronized void run()
	{
		count++;
		if (count == i)
		{
		  System.out.println("count :"+count);//show the final count value
		}
		try 
		{
			Thread.sleep(500);
		} 
		catch (InterruptedException e) 
		{
						e.printStackTrace();
		}
	}
	
}

public class Process 
{
	private static int numthread;
	private static Scanner sc;
	
	public static void main(String[] args) 
	{		
		sc = new Scanner(System.in);
		
		System.out.println("Enter the number of thread you want to create");
		numthread=sc.nextInt();//asking number of threads creation from user
		
		runner t1 = new runner(numthread);
		/*Creating 10 thread */
		for(int i=0; i<numthread; i++) 
		{
			Thread temp = new Thread(t1);
			temp.start();//start threads one by one
			
		}
		
	}

}
