import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ProducerConsumersProblem {
	/*Creating a blocking queue interface having a array class of integer type  */
	private static BlockingQueue<Integer> queue1 = new ArrayBlockingQueue<Integer>(5);
	
	public static void main(String[] args)
	{
		/*Threads1 Array which hold the reference of Threads */
			Thread t1[] = new Thread[3];
			for (int i=0; i<3; i++)
			{
				t1[i] = new Thread(new Runnable() { //Creating Threads object stored reference in arrays
					/*method void run is calling producer method by each thread in a loop */
					public void run() {
						try {
							producer();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					
				});
			t1[i].start();//stating threads one by one using loop
		    }
		/*Threads2 Array which hold the reference of Threads */
			Thread t2[] = new Thread[3];
			for (int j=0; j<3; j++)
			{
				t2[j] = new Thread(new Runnable() {//Creating Threads object stored reference in arrays
					/*method void run is calling producer method by each thread in a loop */
					public void run() {
						try {
							consumer();
						} catch (InterruptedException e) {
							
							e.printStackTrace();
						}
						
					}
				});
				t2[j].start();//stating threads one by one using loop
			}
			/*Threads exceuting in a sequence*/
			for(int k=0; k<3; k++)
			{
				try {
					t1[k].join();
					t2[k].join();
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
			}
	}
	/*method which stored random value b/w 0 to 19  into a blocking queue*/
	public static void producer() throws InterruptedException
	{
		Random r = new Random();
		for( ; ; )
		{
			queue1.put(r.nextInt(20));
		}
	}
	/*method which consume or take value from blocking queue */
	public static void consumer() throws InterruptedException 
	{
		Random r = new Random();
		for( ; ; )
		{
			Thread.sleep(500);
			if((r.nextInt(5) == 2))//checking the numer of generate b/w 0 to 4 is equal to 2
			{
				Integer value = queue1.take();//take that value 
				System.out.println("Taken Value is :"+value+ ";Queue size is :"+queue1.size());//print the value and size of queue
			}
		}
		
	}
	

}
