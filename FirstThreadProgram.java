
public class FirstThreadProgram implements Runnable {
	
	Process p1 ;
	public FirstThreadProgram(Process p1) {
		this.p1 = p1;
	}
/*method which increement the value of variable count*/
   public void increement() throws InterruptedException
	{                      
			
		Process.count++;
	}
  	
public void run()
	  {
		try 
		{
			 increement(); 
			 if(Process.count == Process.threadcount)
			 {
				/*notify to main thread */
				 synchronized (p1)
				 {
					 p1.notify();				 
					
				 }
			 }
	    } 
		catch (InterruptedException e) 
		{
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
}

}
