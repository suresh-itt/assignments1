import java.util.concurrent.BlockingQueue;
public class Consumers implements Runnable {

	private BlockingQueue<Message> queue;
    
    public Consumers(BlockingQueue<Message> q){
        this.queue = q;
    }

    public void run()
    {
        try
        {
            Message msg;
            //consuming messages until exit message is received
            while((msg = queue.take()).getMsg() !="exit"){
            Thread.sleep(10);
            System.out.println("Consumed "+msg.getMsg());
            }
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }

}
