import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
public class BlockQueue {
	public static void main(String[] args) 
	{
        //Creating BlockingQueue of size 10
		
        BlockingQueue<Message> queue = new ArrayBlockingQueue<Message>(10);
        Producer producer = new Producer(queue);
        Consumers consumer = new Consumers(queue);
        
        //create producer to produce messages in queue
        new Thread(producer).start();
        
        //create consumer to consume messages from queue
        
        new Thread(consumer).start();        
        System.out.println("Producer and Consumer has been started");


}
	}